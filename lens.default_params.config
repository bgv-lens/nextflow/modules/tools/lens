params.dummy_file = "${params.ref_dir}/dummy_file"

params.immuno$parse_immuno_manifest$molecule_filter = ''
params.immuno$parse_immuno_manifest$separator = '\t'

# DNA Alignment
params.lens$alignment$manifest_to_dna_alns$fq_trim_tool = "fastp"
params.lens$alignment$manifest_to_dna_alns$fq_trim_tool_parameters = "[]"
params.lens$alignment$manifest_to_dna_alns$aln_tool = "bwa-mem2"
params.lens$alignment$manifest_to_dna_alns$aln_tool_parameters = "[]"
params.lens$alignment$manifest_to_dna_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$alignment$manifest_to_dna_alns$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"

# DNA Alignment Processing
params.lens$alignment$alns_to_dna_procd_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$alignment$alns_to_dna_procd_alns$bed = "${params.ref_dir}/hg38_exome.bed"
params.lens$alignment$alns_to_dna_procd_alns$gtf = ""
params.lens$alignment$alns_to_dna_procd_alns$dup_marker_tool = "bamblaster"
params.lens$alignment$alns_to_dna_procd_alns$dup_marker_tool_parameters = "[]"
params.lens$alignment$alns_to_dna_procd_alns$base_recalibrator_tool = ""
params.lens$alignment$alns_to_dna_procd_alns$base_recalibrator_tool_parameters = "[]"
params.lens$alignment$alns_to_dna_procd_alns$indel_realign_tool = ""
params.lens$alignment$alns_to_dna_procd_alns$indel_realign_tool_parameters = "[]"
params.lens$alignment$alns_to_dna_procd_alns$known_sites_ref = "${params.ref_dir}/Homo_sapiens_assembly38.dbsnp138.vcf.gz"

# RNA Alignment
params.lens$alignment$manifest_to_rna_alns$fq_trim_tool = "fastp"
params.lens$alignment$manifest_to_rna_alns$fq_trim_tool_parameters = "[]"
params.lens$alignment$manifest_to_rna_alns$aln_tool = "star"
params.lens$alignment$manifest_to_rna_alns$aln_tool_parameters = "['star': '--quantMode TranscriptomeSAM --outSAMtype BAM SortedByCoordinate --twopassMode Basic --outSAMunmapped Within']"
params.lens$alignment$manifest_to_rna_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$alignment$manifest_to_rna_alns$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"

# RNA Alignment Processing
params.lens$alignment$alns_to_rna_procd_alns$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$alignment$alns_to_rna_procd_alns$bed = "${params.ref_dir}/hg38_exome.bed"
params.lens$alignment$alns_to_rna_procd_alns$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$alignment$alns_to_rna_procd_alns$dup_marker_tool = ""
params.lens$alignment$alns_to_rna_procd_alns$dup_marker_tool_parameters = "[]"
params.lens$alignment$alns_to_rna_procd_alns$base_recalibrator_tool = ""
params.lens$alignment$alns_to_rna_procd_alns$base_recalibrator_tool_parameters = "[]"
params.lens$alignment$alns_to_rna_procd_alns$indel_realign_tool = ""
params.lens$alignment$alns_to_rna_procd_alns$indel_realign_tool_parameters = "[]"
params.lens$alignment$alns_to_rna_procd_alns$known_sites_ref = "${params.ref_dir}/dummy_file"

# Transcript Quantification from RNA Alignments
params.lens$rna_quant$alns_to_transcript_counts$rna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$rna_quant$alns_to_transcript_counts$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$rna_quant$alns_to_transcript_counts$tx_quant_tool = "salmon"
params.lens$rna_quant$alns_to_transcript_counts$tx_quant_tool_parameters = "[]"

# Somatic Variant Calling
params.lens$somatic$alns_to_som_vars$som_var_caller = "strelka2,mutect2,varscan2"
params.lens$somatic$alns_to_som_vars$som_var_caller_parameters = "['gatk_filter_mutect_calls_suffix': '.gfilt', 'varscan2': '--output-vcf']"
params.lens$somatic$alns_to_som_vars$som_var_caller_suffix = "['mutect2': '.mutect2', 'strelka2': '.strelka2', 'abra2': '.abra2', 'varscan2': '.varscan2']"
params.lens$somatic$alns_to_som_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$somatic$alns_to_som_vars$bed = "${params.ref_dir}/hg38_exome.bed"
params.lens$somatic$alns_to_som_vars$som_var_pon_vcf = "${params.ref_dir}/1000g_pon.hg38.vcf.gz"
params.lens$somatic$alns_to_som_vars$som_var_af_vcf = "${params.ref_dir}/af-only-gnomad.hg38.vcf.gz"
params.lens$somatic$alns_to_som_vars$known_sites_ref = "${params.ref_dir}/small_exac_common_3.hg38.vcf.gz"
params.lens$somatic$alns_to_som_vars$species = "homo sapiens"

# Somatic Variant Filtering
params.lens$somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool = 'bcftools'
params.lens$somatic$som_vars_to_filtd_som_vars$vcf_filtering_tool_parameters = "[]"

# SNV/InDel Filtering
params.lens$snpsift_filter_snvs$snpsift_snv_filter_parameters = "ANN[*].EFFECT has 'missense_variant'"
params.lens$snpsift_filter_indels$snpsift_indel_filter_parameters = "(ANN[*].EFFECT has 'conservative_inframe_insertion') || (ANN[*].EFFECT has 'conservative_inframe_deletion') || (ANN[*].EFFECT has 'disruptive_inframe_insertion') || (ANN[*].EFFECT has 'disruptive_inframe_deletion') || (ANN[*].EFFECT has 'frameshift_variant')"

# Somatic InDel Normalization
params.lens$somatic$som_vars_to_normd_som_vars$vcf_norming_tool = 'bcftools'
params.lens$somatic$som_vars_to_normd_som_vars$vcf_norming_tool_parameters = "[]"
params.lens$somatic$som_vars_to_normd_som_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"

# Somatic Variant Merging
params.lens$somatic$combine_strategy = "union"
# Intersection
params.lens$somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool = "bedtools"
params.lens$somatic$som_vars_to_isecd_som_vars$vcf_isecing_tool_parameters = "[]"
# Union
params.lens$somatic$som_vars_to_union_som_vars$vcf_merging_tool = 'jacquard_merge'
params.lens$somatic$som_vars_to_union_som_vars$vcf_merging_tool_parameters = "['jacquard_merge': '--include_format_tags=\"GT,AF,AU,CU,GU,TU,TAR,TIR,FREQ,VAF\"']"

# Variant Annotation
params.lens$snpeff$annot_tool_ref = "${params.ref_dir}/GRCh38.GENCODEv37"

# Germline Variant Calling
params.lens$germline$alns_to_germ_vars$germ_var_caller = 'deepvariant'
params.lens$germline$alns_to_germ_vars$germ_var_caller_parameters = "['deepvariant': '--model_type WES']"
params.lens$germline$alns_to_germ_vars$germ_var_caller_suffix = "['deepvariant': '.deepv']"
params.lens$germline$alns_to_germ_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$germline$alns_to_germ_vars$bed = "${params.ref_dir}/hg38_exome.bed"

# Germline Variant Filtering
params.lens$germline$germ_vars_to_filtd_germ_vars$vcf_filtering_tool = 'bcftools'
params.lens$germline$germ_vars_to_filtd_germ_vars$vcf_filtering_tool_parameters = "[]"

# Misc Variant
params.lens$bcftools$bcftools_index$bcftools_index_parameters = ''
params.lens$bcftools$bcftools_index_somatic$bcftools_index_somatic_parameters = ''

# Somatic + Germline Merging
params.lens$seq_variation$germ_and_som_vars_to_tumor_vars$vcf_merge_tool = 'jacquard_merge'
params.lens$seq_variation$germ_and_som_vars_to_tumor_vars$vcf_merge_tool_parameters = "[]"

# Phasing of Somatic + Germline Variants
params.lens$seq_variation$make_phased_tumor_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$seq_variation$make_phased_tumor_vars$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$seq_variation$make_phased_tumor_vars$species = 'human'
params.lens$seq_variation$make_phased_tumor_vars$var_phaser_tool = 'whatshap'
params.lens$seq_variation$make_phased_tumor_vars$var_phaser_tool_parameters = "['whatshap': '--ignore-read-groups']"

# Phasing of Germline Variants
params.lens$seq_variation$make_phased_germline_vars$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$seq_variation$make_phased_germline_vars$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$seq_variation$make_phased_germline_vars$var_phaser_tool = 'whatshap'
params.lens$seq_variation$make_phased_germline_vars$var_phaser_tool_parameters = "['whatshap': '--ignore-read-groups']"

# MHC Calling
params.lens$immuno$procd_fqs_to_mhc_alleles$aln_tool = ""
params.lens$immuno$procd_fqs_to_mhc_alleles$aln_tool_parameters = '--outSAMtype BAM SortedByCoordinate'
params.lens$immuno$procd_fqs_to_mhc_alleles$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool = 'seq2hla'
params.lens$immuno$procd_fqs_to_mhc_alleles$mhc_caller_tool_parameters = "[]"

# TCR Repertoire
params.lens$immuno$procd_fqs_to_tcr_repertoire$tcr_rep_tool = ''
params.lens$immuno$procd_fqs_to_tcr_repertoire$tcr_rep_tool_paraneters = "[]"

# Splice Variants
params.splice$alns_to_splice_variants$splice_var_caller = 'neosplice'
params.splice$alns_to_splice_variants$splice_var_caller_parameters = "['neosplice_augmented_splice_graph_build': '--min-variants 10 --min-coverage 10 --cutoff 0.000005', 'neosplice_kmer_search_bwt': '--tumor_threshold 250 --normal_threshold 1', 'neosplice_kmer_graph_inference': '--transcript_min_coverage 100']"
params.splice$alns_to_splice_variants$splice_var_caller_ref = "['neosplice': \"${params.ref_dir}/peptidome.homo_sapiens\"]"
params.splice$alns_to_splice_variants$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.splice$alns_to_splice_variants$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.splice$alns_to_splice_variants$gff = "${params.ref_dir}/gencode.v37.annotation.gff3"
params.splice$alns_to_splice_variants$species = "homo sapiens"

# Viral Detection
params.lens$viral$alns_to_viruses$viral_workflow = 'virdetect'
params.lens$viral$alns_to_viruses$viral_workflow_parameters = "[]"
params.lens$viral$alns_to_viruses$viral_ref = "${params.ref_dir}/virus_masked_hg38.fa"
params.lens$viral$unaligned_fqs_to_virdetect_cds_counts$viral_cds_ref = "${params.ref_dir}/virus.cds.fa"

# Gene Fusion Calling
params.lens$fusion$procd_fqs_to_fusions$fusion_tool = 'starfusion'
params.lens$fusion$procd_fqs_to_fusions$fusion_tool_parameters = "['starfusion': '--examine_coding_effect']"
params.lens$fusion$procd_fqs_to_fusions$fusion_ref = "['starfusion': \"${params.ref_dir}/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play\"]"
params.lens$fusion$procd_fqs_to_fusions$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$fusion$procd_fqs_to_fusions$gtf  = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"

# Tumor Purity
params.lens$onco$alns_to_tumor_purities$tx_quant_tool = ''
params.lens$onco$alns_to_tumor_purities$tx_quant_tool_parameters = "[]"
params.lens$onco$alns_to_tumor_purities$tumor_purities_tool = 'sequenza'
params.lens$onco$alns_to_tumor_purities$tumor_purities_tool_parameters = "[sequenza_gc_wiggle: '-w 50', sequenza_bam2seqz: '', sequenza_seqz_binning: '-w 50']"
params.lens$onco$alns_to_tumor_purities$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$onco$alns_to_tumor_purities$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$onco$alns_to_tumor_purities$bed = "${params.ref_dir}/hg38_exome.bed"

# CNA and cancer cell fraction
params.lens$onco$alns_to_cnas$cna_tool = 'cnvkit'
params.lens$onco$alns_to_cnas$cna_tool_refs = ''
params.lens$onco$alns_to_cnas$cna_tool_parameters = "[cnvkit_segment: '--drop-low-coverage', cnvkit_call: '--ploidy 2']"
params.lens$onco$alns_to_cnas$aln_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$onco$alns_to_cnas$bed = "${params.ref_dir}/hg38_exome.bed"
params.lens$onco$alns_to_cnas$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"


# CTA/Self-antigens Neoantigen Workflow
params.lens$neos$selfs_to_neos$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$neos$selfs_to_neos$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$neos$selfs_to_neos$cta_self_gene_list = "${params.ref_dir}/cta_and_self_antigen.homo_sapiens.gene_list"
params.lens$neos$selfs_to_neos$samtools_index_parameters = ''
params.lens$neos$selfs_to_neos$lenstools_filter_expressed_self_parameters = '-p 95'
params.lens$neos$selfs_to_neos$lenstools_get_expressed_self_bed_parameters = ''
params.lens$neos$selfs_to_neos$samtools_faidx_fetch_parameters = ''
params.lens$neos$selfs_to_neos$bcftools_index_parameters = ''

# ERVs Neoantigen Workflow
params.lens$neos$ervs_to_neos$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$neos$ervs_to_neos$geve_general_ref = "${params.ref_dir}/Hsap38.txt"
params.lens$neos$ervs_to_neos$lenstools_get_expressed_ervs_bed_parameters = ""
params.lens$neos$ervs_to_neos$lenstools_make_erv_peptides_parameters = ""
params.lens$neos$ervs_to_neos$lenstools_filter_expressed_ervs_parameters = ""
params.lens$neos$ervs_to_neos$lenstools_filter_ervs_by_rna_coverage_parameters = "--mean-depth 60"
params.lens$neos$ervs_to_neos$normal_control_quant = ""
params.lens$neos$ervs_to_neos$tpm_threshold = ""

# SNVs Neoantigen Workflow
params.lens$neos$snvs_to_neos$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$neos$snvs_to_neos$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$neos$snvs_to_neos$pep_ref = "${params.ref_dir}/gencode.v37.pc_translations.fa"
params.lens$neos$snvs_to_neos$som_var_type = "SNV"
params.lens$neos$snvs_to_neos$lenstools_filter_expressed_variants_parameters = "-p 75"
params.lens$neos$snvs_to_neos$bcftools_index_phased_germline_parameters = ""
params.lens$neos$snvs_to_neos$bcftools_index_phased_tumor_parameters = ""
params.lens$neos$snvs_to_neos$lenstools_get_expressed_transcripts_bed_parameters = ""
params.lens$neos$snvs_to_neos$samtools_faidx_fetch_somatic_folder_parameters = ""

# InDels Neoantigen Workflow
params.lens$neos$indels_to_neos$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$neos$indels_to_neos$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$neos$indels_to_neos$pep_ref = "${params.ref_dir}/gencode.v37.pc_translations.fa"
params.lens$neos$indels_to_neos$som_var_type = "InDel"
params.lens$neos$indels_to_neos$lenstools_filter_expressed_variants_parameters = "-p 75"
params.lens$neos$indels_to_neos$bcftools_index_phased_germline_parameters = ""
params.lens$neos$indels_to_neos$bcftools_index_phased_tumor_parameters = ""
params.lens$neos$indels_to_neos$lenstools_get_expressed_transcripts_bed_parameters = ""
params.lens$neos$indels_to_neos$samtools_faidx_fetch_somatic_folder_parameters = ""

# Fusion Neoantigen Workflow
params.lens$neos$fusions_to_neos$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$neos$fusions_to_neos$dna_ref = "${params.ref_dir}/Homo_sapiens.assembly38.no_ebv.fa"
params.lens$neos$fusions_to_neos$bedtools_index_phased_germline_parameters = ''
params.lens$neos$fusions_to_neos$lenstools_get_fusion_transcripts_bed_parameters = ''
params.lens$neos$fusions_to_neos$samtools_faidx_fetch_parameters = ''

# Viral Neoantigen Workflow
params.lens$neos$viruses_to_neos$viral_cds_ref = "${params.ref_dir}/virus.cds.fa"
params.lens$neos$viruses_to_neos$lenstools_filter_expressed_viruses_parameters = ""
params.lens$neos$viruses_to_neos$lenstools_filter_viruses_by_rna_coverage_parameters = "--coverage 50"
params.lens$neos$viruses_to_neos$lenstools_get_expressed_viral_bed_parameters = ""
params.lens$neos$viruses_to_neos$lenstools_make_viral_peptides_parameters = ""

# pMHC Summarization
params.lens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool = 'netmhcpan,netmhcstabpan,mhcflurry'
params.lens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool_parameters = '[]'
params.lens$immuno$peps_and_alleles_to_antigen_stats$antigen_tool_ref_dir = "['mhcflurry': \"${params.ref_dir}/mhcflurry\", 'antigen_garnish': \"${params.ref_dir}/antigen.garnish\"]"
params.lens$immuno$peps_and_alleles_to_antigen_stats$species = 'human'
params.lens$immuno$peps_and_alleles_to_antigen_stats$peptide_lengths = '8,9,10,11'

# Agretopicity Calculation
params.lens$immuno$calculate_agretopicity$blastp_db_dir = "${params.ref_dir}/antigen.garnish"
params.lens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool = 'netmhcpan,mhcflurry'
params.lens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool_parameters = '[]'
params.lens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$antigen_tool_ref_dir = "['mhcflurry': \"${params.ref_dir}/mhcflurry\", 'antigen_garnish': \"${params.ref_dir}/antigen.garnish\"]"
params.lens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$species = 'human'
params.lens$immuno$calculate_agretopicity$peps_and_alleles_to_antigen_stats$peptide_lengths = '8,9,10,11'

# Peptide Quantification
params.lens$lenstools$lenstools_get_snv_peptide_read_count$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"
params.lens$lenstools$lenstools_get_indel_peptide_read_count$gtf = "${params.ref_dir}/gencode.v37.annotation.with.hervs.gtf"

# Non-CTA Self Peptide Filtering
params.lens$lenstools$lenstools_filter_mutant_peptides$pep_ref = "${params.ref_dir}/gencode.v37.pc_translations.fa"

# pMHC Filtering
params.lens$lenstools$lenstools_annotate_pmhcs$binding_affinity_threshold = 1000

# Relevant Metadata
params.lens$lenstools$annotate_ctas$cta_external_ref = "${params.ref_dir}/canonical_txs.mtec.norm.subcell.annot.tsv"
params.lens$lenstools$annotate_ervs$erv_external_ref = "${params.ref_dir}/erv_scores.25SEP2023.tsv"

# LENS output directory
params.lens_out_dir = "${params.output_dir}/lens"
